<?php
namespace app\admin\controller;
//use think\View;
use think\Db;
class AuthGroup extends Common
{

    public function index() {
        return $this->fetch();
    }
    /*
     * 新增管理员
     */
    public function add() {

        $admin_model = model('Admin');
        if(request()->isPost()){
            $username = input('post.username');
            $email = input('post.email');
            $pwd_salt = bornPassword();
            //dump($pwd_salt);
            $password = $pwd_salt[0];
            $salt = $pwd_salt[1];
            //dump($password);dump($salt);die;

            $data = [
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'salt' => $salt
            ];
            $check_msg = $admin_model->admin_validate($data);
            if($check_msg != 1){
                $this->error($check_msg);
            }else{
                if(db('admin')->insert($data)){
                    $this->success('新增用户成功！',url('lst'));
                }else{
                    $this->error('新增用户失败');
                }
            };
            return;
        }
        return $this->fetch();
    }
    /*
     * 删除管理员
     */
    public function del($id) {
        if(db('admin')->where('id',$id)->delete()){
            $this->success('删除成功',url('lst'));
        }else{
            $this->error('删除失败');
        }
    }
    /*
     * 编辑管理员
     */

    public function edit($id) {
        $admin_model = model('admin');
        if(request()->isPost()){
            //$username = input('post.username');
            $email = input('post.email');
            $data = [
                //'username' => $username,
                'email' => $email
            ];
            $check_message = $admin_model->admin_validate($data,'edit');
            if($check_message != 1){
                $this->error();
            }else{
                if(db('admin')->where('id',$id)->update($data)){
                    $this->success('保存信息成功','lst');
                }else{
                    $this->error('保存信息失败');
                };
            };
            return;
        }
        $admin = Db::name('admin')->where('id',$id)->field('username,email')->find();
        $this->assign('admin',$admin);
        return $this->fetch();
    }

    public function logout () {
        session(null);
    }


}
