<?php
namespace app\admin\controller;
//use think\View;
use think\Controller;
use think\Db;

class Article extends Common
{
    /*
     * 文章管理首页
     */
    public function index() {
        //$data = input('post.');dump($data);die;
        $articleres = db('article')->where(1)->order('id','desc')->paginate(2);
        $this->assign('articleres',$articleres);
//        dump($articleres);die;
        $this->assign('page',$articleres->render());
        return $this->fetch();
    }
    /*
     * 发布文章
     */
    public function add() {
        if(request()->isPost()){
            $data = input('post.');
            if(input('file.')){
                $path = $this->upload_one('thumbnail');
                if($path['status']==0) $this->error($path['error_info']);
                $data['thumbnail'] = $path['uri'];
            }

            if($data['title'] == '') $this->error('文章标题不能为空');
            $res = db('article')->insert($data);
            if($res<1){
                $this->error('发布文章失败');
            }else{
                $this->success('发布文章成功',url('index'));
            }
            return;
        }
        $cate_model = model('Cate');
        $cateres = $cate_model->cateTree();
        $this->assign('cateres',$cateres);
        return $this->fetch();
    }
    /*
     * 上传单文件
     */
    public function upload_one($filename){
        $file = request()->file($filename);
        // 移动到框架应用根目录/public/uploads/ 目录下
        $info = $file->validate(['size'=>2048000,'ext'=>'jpg,png,gif'])->move(ROOT_PATH . 'public' . DS . 'uploads');
        if($info){
            $path_name =  $info->getPathName();
            $position = strpos($path_name,'\public');
            $path_name = substr($path_name,$position);
            return ['status'=>1,'uri'=>$path_name];
        }else{
            // 上传失败获取错误信息
             $error_info = $file->getError();
            return ['status'=>0,'error_info'=>$error_info];
        }
    }
   /*
    * 删除文章
    */
    public function del($id) {
        if(db('article')->where('id',$id)->delete()){
            $this->success('删除成功',url('index'));
        }else{
            $this->error('删除失败');
        }
    }
    /*
     * 编辑文章
     */
    public function edit($id) {
        if(request()->isPost()){
            $data = input('post.');
            if($data['title'] == '') $this->error('文章标题不能为空');
            if(input('file.')){
                $path = $this->upload_one('thumbnail');
                if($path['status']==0) $this->error($path['error_info']);
                $data['thumbnail'] = $path['uri'];
            }
            $res = db('article')->where('id',$id)->update($data);
            if($res<1){
                $this->error('更新文章失败');
            }else{
                $this->success('更新文章成功',url('index'));
            }
            return;
        }
        $articles = db('article')->where('id',$id)->find();
        $this->assign('articles',$articles);
        $cate_model = model('Cate');
        $cateres = $cate_model->cateTree();
        $this->assign('cateres',$cateres);
        return $this->fetch();
    }



}
