<?php
namespace app\admin\controller;
//use think\View;
use think\Controller;
use think\Db;

class Login extends Controller
{
    public function index () {

        return $this->fetch();
    }

    public function login() {
        if(!request()->isAjax()) $this->error('非法访问');
        $username = input('request.username');
        $password = input('request.password');
        //return ['user'=>$username,'password'=>$password];
        $res = db('admin')->where('username',$username)->find();
//        if(!$res) $this->error('用户名不存在');
        if(!$res) return ['status'=>0,'message'=>'用户名不存在'];
        //if(!$res) return [ 'status'=>0,'message'=>'用户名不存在'];
        $password = bornPassword($password,$res['salt']);
        $password = $password[0];
        if($password != $res['password']) return ['status'=>0,'message'=>'用户或密码错误'];
        //if($password != $res['password']) $this->error('用户或密码错误');
        session('name', $username);
        session('id', $res['id']);
        //$this->success('登录成功',url('admin/lst'));
        return ['status'=>1,'message'=>'登录成功,跳转中...','url'=>'http://'.$_SERVER['HTTP_HOST'].url('admin/lst')];
        //return $this->fetch();
    }

    

}
