<?php
namespace app\admin\controller;
//use think\View;
use think\Controller;
use think\Db;

class Cate extends Common
{

    public function index() {
        $cate_model = model('Cate');
        $cateres = $cate_model->cateTree();
        $this->assign('cateres',$cateres);

        return $this->fetch();
    }
    /*
     * 新增管理用户
     */
    public function add() {
        if(request()->isAjax()){
            $data = input('request.');
            if($data['name'] == '') return['status'=>0,'error_message'=>'栏目名称不能为空'];
            $res = db('cate')->insert($data);
            if($res<1){
                return['status'=>0,'error_message'=>'添加栏目失败'];
            }else{
                return['status'=>1,'message'=>'添加栏目成功,跳转中...','url'=>url('index')];
            }
        }
        $cate_model = model('Cate');
        $cateres = $cate_model->cateTree();
//        dump($cateres);die;
        $this->assign('cateres',$cateres);
        return $this->fetch();
    }

    public function del($id) {
        if(db('article')->where('cateid',$id)->find()){
            $this->error('删除栏目，请先清空属于此栏目的文章');
        }
        if(db('admin')->where('id',$id)->delete()){
            $this->success('删除成功',url('lst'));
        }else{
            $this->error('删除失败');
        }
    }


    public function edit($id) {
        if(request()->isPost()){
            $data = input('post.');
            if($data['name'] == '') $this->error('栏目名称不能为空');
            $res = db('cate')->where('id',$id)->update($data);
            if($res<1){
                $this->error('更新栏目失败');
            }else{
                $this->success('更新栏目成功',url('index'));
            }
            return;
        }
        $cate_model = model('Cate');
        $cateres = $cate_model->cateTree();
        $this->assign('cateres',$cateres);
        $cates = $cate_model->find($id)->getData();
//        dump($cates);die;
        $this->assign('cates',$cates);
        return $this->fetch();
    }



}
