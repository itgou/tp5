<?php
namespace app\admin\model;

use think\Model;
use think\Validate;

class Cate extends Model {
    protected $resultSetType = '';
    /*
     * @param $data 需要验证的数据
     * 验证数据
     */
    public function admin_validate ( $data,$scene='' ){

    }

    public function cateTree(){
        $data = $this->select();
        $cate_data = [];
        foreach ($data as $k => $v){
            $cate_data[$k] = $v->getData();
        }
        return $this->_cateTree($cate_data);
    }

    public function _cateTree($cate_data,$pid=0,$level=0){
        static $arr = [];
        foreach ($cate_data as $k => $v){
            if($v['pid'] == $pid){
                $v['level'] = $level;
                $arr[] = $v;
                $this->_cateTree($cate_data,$v['id'],$level+1);
            }
        }
        return $arr;
    }
}