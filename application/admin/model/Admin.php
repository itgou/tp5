<?php
namespace app\admin\model;

use think\Model;
use think\Validate;

class Admin extends Model {
    protected $resultSetType = '';
    /*
     * @param $data 需要验证的数据
     * 验证数据
     */
    public function admin_validate ( $data,$scene='' ){
        //dump($data);dump($scene);die;
        $rule = [
            'username'  => 'require|unique:admin|max:20',
            'email' => 'email'
        ];

        $msg = [
            'username.require' => '请输入用户名',
            'username.max'     => '用户名最多不能超过20个字符',
            'username.unique'     => '用户名已存在，请重新输入',
            'email'        => '邮箱格式错误',
        ];

        $validate = new Validate($rule,$msg);
        $validate->scene('edit', ['email']);
            if (!$validate->scene($scene)->check($data)) {
                return $validate->getError();
            }else{
                return 1;
            }

    }

    public function get_all_admin(){
        $aa = $this->where(1)->order('id','desc')->field('username,email')->select();
        $admins = [];
        foreach ($aa as $k => $v){
            $admins[]=$v->getData();
        }
        return $admins;
    }
}